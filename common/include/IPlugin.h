#pragma once
#include <string>

typedef struct
{
    int Major = 0;
    int Minor = 0;
}PluginVersion;

class IPlugin
{
public:
    virtual ~IPlugin() = default;

    virtual std::string getName() const = 0;
    virtual PluginVersion getVersion() const = 0;

    virtual void PrintMsg(const std::string &s) = 0;
};